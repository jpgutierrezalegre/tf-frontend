(function initApp() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'users': '/users',
      'user-detail': '/user-detail',
      'accounts': '/accounts',
      'account-add': '/account-add',
      'account-details': '/account-details/:account',
      'movement-create': '/movement-create/:account',
      'account-operation': '/account-details/:account/operation',
    }
  });
}());
