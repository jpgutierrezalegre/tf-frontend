import { html, css } from 'lit-element';
import { CellsElement } from '@cells/cells-element';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-help-modal';

import { setData, getUsername } from '../auth-dm/auth-dm';

export class HeaderComponent extends i18n(CellsElement) {
  static get is() {
    return 'header-component';
  }

  constructor() {
    super();
    this.subscribe('loginSuccess', (loginData) => {
      this.username = loginData.name;
      setData(JSON.stringify(loginData));
    });

    this.username = getUsername();
  }

  static get properties() {
    return {
      username: { type: String },
      hideBack: {
        type: Boolean,
        attribute: 'hide-back',
      }
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._logoutModal = this.shadowRoot.querySelector('#logoutModal');
  }

  _handleLogout() {
    sessionStorage.clear();
    window.cells.logout();
  }

  get backProperties() {
    return this.hideBack ? {} : {
      'icon-left1': 'coronita:return-12',
      'accessibility-text-icon-left1': 'Volver',
      '@header-icon-left1-click': () => this.handleBackClick()
    };
  }

  render() {
    return html`
      <div class="app-header">
        <bbva-header-main icon-right1="coronita:on" accessibility-text-icon-right1="Cerrar Sesión"
          @header-icon-right1-click=${() => this._logoutModal.open()}
          ...="${spread(this.backProperties)}"
          text=${this.t('dashboard-page.header', '', { name: this.username })}>
        </bbva-header-main>
      </div>

      <bbva-help-modal id="logoutModal" header-icon="coronita:info"
        header-text=${this.t('dashboard-page.logout-modal.header')}
        button-text=${this.t('dashboard-page.logout-modal.button')} 
        @help-modal-footer-button-click=${() => this._handleLogout()}
        style="z-index: 1000">
        <div slot="slot-content">
          <span>${this.t('dashboard-page.logout-modal.slot')}</span>
        </div>
      </bbva-help-modal>
    `;
  }

  handleBackClick() {
    this.dispatchEvent(
      new CustomEvent('header-back-click', {
        bubbles: true,
      }),
    );
  }

  static get styles() {
    return css`
      .app-header {
        position: sticky;
        top: 0;
        z-index: 1;
      }
    `;
  }
}

customElements.define(HeaderComponent.is, HeaderComponent);