const LOGIN_MOCK = [
  {
    'token': 'TOKEN_MOCK_ADMIN',
    'isAdmin': true,
    'name': 'Mr. Admin',
    'msg': 'this is a message'
  },
  {
    'token': 'TOKEN_MOCK_USER',
    'isAdmin': false,
    'name': 'Usu Ario',
    'msg': 'this is a message'
  },
];

import * as apiDm  from '../utils/api-dm.js';

export async function login(documentNumber, password) {

  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, documentNumber === 'admin' ? LOGIN_MOCK[0] : LOGIN_MOCK[1]);
  } else {
    const loginData = {
      documentNumber: documentNumber,
      password: password
    };
    const response = await apiDm.postWithAuth('/login', loginData);
    return response;
  }

}
