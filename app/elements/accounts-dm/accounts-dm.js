const MOCK_ACCOUNTS = [
  {
    'accountNumber': '100000001',
    'userId': 2,
    'currency': 'PEN',
    'balance': 255.58,
    'openingDate': {
      '$date': '2019-10-07T02:54:37.644Z'
    },
    'status': 1,
    'movements': [
      {
        'originAccount': '100000001',
        'destinationAccount': '100000002',
        'currency': 'PEN',
        'amount': 100,
        'date': {
          '$date': '2020-04-03T02:54:37.644Z'
        },
        'reference': 'Test 1'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000004',
        'currency': 'PEN',
        'amount': 29.9,
        'date': {
          '$date': '2020-05-04T02:54:37.644Z'
        },
        'reference': 'Test 2'
      },
      {
        'originAccount': '100000003',
        'destinationAccount': '100000001',
        'currency': 'PEN',
        'amount': 50.5,
        'date': {
          '$date': '2020-06-05T02:54:37.644Z'
        },
        'reference': 'Test 3'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000002',
        'currency': 'PEN',
        'amount': 100,
        'date': {
          '$date': '2020-04-03T02:54:37.644Z'
        },
        'reference': 'Test 1'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000004',
        'currency': 'PEN',
        'amount': 29.9,
        'date': {
          '$date': '2020-05-04T02:54:37.644Z'
        },
        'reference': 'Test 2'
      },
      {
        'originAccount': '100000003',
        'destinationAccount': '100000001',
        'currency': 'PEN',
        'amount': 50.5,
        'date': {
          '$date': '2020-06-05T02:54:37.644Z'
        },
        'reference': 'Test 3'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000002',
        'currency': 'PEN',
        'amount': 100,
        'date': {
          '$date': '2020-04-03T02:54:37.644Z'
        },
        'reference': 'Test 1'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000004',
        'currency': 'PEN',
        'amount': 29.9,
        'date': {
          '$date': '2020-05-04T02:54:37.644Z'
        },
        'reference': 'Test 2'
      },
      {
        'originAccount': '100000003',
        'destinationAccount': '100000001',
        'currency': 'PEN',
        'amount': 50.5,
        'date': {
          '$date': '2020-06-05T02:54:37.644Z'
        },
        'reference': 'Test 3'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000002',
        'currency': 'PEN',
        'amount': 100,
        'date': {
          '$date': '2020-04-03T02:54:37.644Z'
        },
        'reference': 'Test 1'
      },
      {
        'originAccount': '100000001',
        'destinationAccount': '100000004',
        'currency': 'PEN',
        'amount': 29.9,
        'date': {
          '$date': '2020-05-04T02:54:37.644Z'
        },
        'reference': 'Test 2'
      },
      {
        'originAccount': '100000003',
        'destinationAccount': '100000001',
        'currency': 'PEN',
        'amount': 50.5,
        'date': {
          '$date': '2020-06-05T02:54:37.644Z'
        },
        'reference': 'Test 3'
      }
    ]
  },
  {
    'accountNumber': '100000002',
    'userId': 3,
    'currency': 'PEN',
    'balance': 869.39,
    'openingDate': {
      '$date': '2019-12-11T02:54:37.644Z'
    },
    'status': 1,
    'movements': []
  },
  {
    'accountNumber': '100000003',
    'userId': 4,
    'currency': 'PEN',
    'balance': 0.00,
    'openingDate': {
      '$date': '2020-06-01T02:54:37.644Z'
    },
    'status': 0,
    'movements': []
  },
  {
    'accountNumber': '100000004',
    'userId': 4,
    'currency': 'PEN',
    'balance': 0.00,
    'openingDate': {
      '$date': '2020-07-21T02:54:37.644Z'
    },
    'status': 1,
    'movements': []
  }
];

import * as apiDm  from '../utils/api-dm.js';

export async function getAccounts() {

  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, MOCK_ACCOUNTS);
  } else {
    const response = await apiDm.getWithAuth('/accounts');
    return response;
  }
}

export async function getAccountsFromUser(userId) {

  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, MOCK_ACCOUNTS.filter(a=>a.userId.toString() === userId));
  } else {
    const response = await apiDm.getWithAuth('/accounts?userId=' + userId);
    return response;
  }
}

export async function createAccount(data) {
  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(201, { msg: 'Cuenta creada', accountNumber: data.accountNumber });
  } else {
    const response = await apiDm.postWithAuth('/accounts', data);
    return response;
  }
}

export async function deleteAccount(accountNumber) {
  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, { msg: 'Cuenta desactivada', accountNumber: accountNumber });
  } else {
    const response = await apiDm.deleteWithAuth('/accounts/' + accountNumber);
    return response;
  }
}

export async function getAccountOwner(accountNumber) {

  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, {
      'firstName': 'Mock',
      'middleName': 'Owner',
      'lastName': 'Mock',
      'secondLastName': 'Owner',
    });
  } else {
    const response = await apiDm.getWithAuth('/accounts/' + accountNumber + '/owner');
    return response;
  }
}

export async function getAccountDetails(accountNumber) {
  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, MOCK_ACCOUNTS.find(e=>e.accountNumber === accountNumber.toString()));
  } else {
    const response = await apiDm.getWithAuth('/accounts/' + accountNumber);
    return response;
  }
}

export async function createAccountOperation(accountNumber, data) {
  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, {msg: 'Operación mock creada'});
  } else {
    const response = await apiDm.patchWithAuth('/accounts/' + accountNumber, data);
    return response;
  }

}

export async function createMovement(accountNumber, data) {
  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(201, {msg: 'Movimiento mock creado'});
  } else {
    const response = await apiDm.postWithAuth('/accounts/' + accountNumber + '/movements', data);
    return response;
  }
}