import { getToken } from '../auth-dm/auth-dm.js';

export async function buildMockResponse(status, data) {
  const mockResponse = {
    status: status,
    json: function() {
      return new Promise((resolve) => {
        resolve(data);
      });
    }
  };

  const timer = Math.floor(Math.random() * (1500 - 500 + 1)) + 500;

  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockResponse);
    }, timer);
  });

}

export async function getWithAuth(apiPath) {

  const { host, path } = window.AppConfig;

  return fetch(host + path + apiPath, {
    method: 'GET',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    }
  });
}

export async function postWithAuth(apiPath, data) {

  const { host, path } = window.AppConfig;

  return fetch(host + path + apiPath, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    },
    body: JSON.stringify(data)
  });
}

export async function putWithAuth(apiPath, data) {

  const { host, path } = window.AppConfig;

  return fetch(host + path + apiPath, {
    method: 'PUT',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    },
    body: JSON.stringify(data)
  });
}

export async function deleteWithAuth(apiPath) {

  const { host, path } = window.AppConfig;

  return fetch(host + path + apiPath, {
    method: 'DELETE',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    }
  });
}

export async function patchWithAuth(apiPath, data) {

  const { host, path } = window.AppConfig;

  return fetch(host + path + apiPath, {
    method: 'PATCH',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    },
    body: JSON.stringify(data)
  });
}
