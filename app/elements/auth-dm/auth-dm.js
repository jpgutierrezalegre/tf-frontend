const AUTH_STORAGE_NAME = 'techu_auth';

export function setData(data) {
  sessionStorage.setItem(AUTH_STORAGE_NAME, data);
}

export function isLoggedIn() {
  return sessionStorage.getItem(AUTH_STORAGE_NAME) !== null;
}

export function isAdmin() {
  return JSON.parse(sessionStorage.getItem(AUTH_STORAGE_NAME)).isAdmin;
}

export function getToken() {
  if (isLoggedIn()) {
    return 'Bearer ' + JSON.parse(sessionStorage.getItem(AUTH_STORAGE_NAME)).token;
  }
  return null;
}

export function getUsername() {
  if (isLoggedIn()) {
    return JSON.parse(sessionStorage.getItem(AUTH_STORAGE_NAME)).name;
  }
  return null;
}