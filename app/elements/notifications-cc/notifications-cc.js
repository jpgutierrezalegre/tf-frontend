import { html, css } from 'lit-element';
import { CellsElement } from '@cells/cells-element';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-notification-toast';
import '@bbva-web-components/bbva-progress-content';

export class NotificationComponent extends i18n(CellsElement) {
  static get is() {
    return 'notifications-component';
  }

  constructor() {
    super();

    this.subscribe('display-notification', (notificationData) => {
      this.notificationData = notificationData;
      setTimeout(()=>{
        this.notificationData = null;
      }, 3000);
    });

    this.subscribe('handle-error', errorResponse => {
      if (errorResponse.status === 401) {
        // unathorized
        this.publish('display-notification', { type: 'error', icon: 'coronita:alert', text: 'Sesión finalizada'});
      } else {
        errorResponse.json().then(jsonError => {
          this.publish('display-notification', { type: 'error', icon: 'coronita:alert', text: jsonError.msg});
        }).catch(err=>{});
      }
    });

    this.subscribe('display-loading', (display)=>{
      this.displayLoading = display;
    });

  }

  static get properties() {
    return {
      notificationData: { type: Object },
      displayLoading: { type: Boolean, default: false }
    };
  }

  get notificationProperties() {
    if (!this.notificationData) {
      return {};
    }

    return {
      'alert-text': this.notificationData.text,
      'show': '',
      'type': this.notificationData.type,
      'icon-explainer': this.notificationData.icon || 'coronita:info'
    };
  }

  get spinnerProperties() {
    return {
      class: this.displayLoading ? 'visibleLoading' : 'hiddenLoading'
    };
  }

  render() {
    return html`
      <div class="app-header">
        <bbva-notification-toast  ...="${spread(this.notificationProperties)}"></bbva-notification-toast>
      </div>
      <div>
        <bbva-progress-content ...="${spread(this.spinnerProperties)}" >Loading</bbva-progress-content>
      </div>
    `;
  }

  static get styles() {
    return css`
      .app-header {
        position: sticky;
        top: 0;
        z-index: 10;
        width: 99%;
      }

      .visibleLoading {
        z-index: 100;
        opacity: 75%;
        display: flex !important;
      }

      .hiddenLoading {
        display: none
      }
    `;
  }

}

customElements.define(NotificationComponent.is, NotificationComponent);