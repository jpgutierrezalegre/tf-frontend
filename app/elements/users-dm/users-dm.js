const MOCK_USERS = [
  {
    'id': 1,
    'documentNumber': 'admin',
    'firstName': 'Layne',
    'middleName': 'Glynnis',
    'lastName': 'Paaso',
    'secondLastName': 'Denson',
    'email': 'gdenson0@reddit.com',
    'isAdmin': true
  },
  {
    'id': 2,
    'documentNumber': '16766367',
    'firstName': 'Aeriela',
    'middleName': 'Fulton',
    'lastName': 'Dudney',
    'secondLastName': 'Yakob',
    'email': 'fyakob1@omniture.com',
    'isAdmin': false
  },
  {
    'id': 3,
    'documentNumber': '44518787',
    'firstName': 'Anna',
    'middleName': 'Dasi',
    'lastName': 'Enevoldsen',
    'secondLastName': 'Teacy',
    'email': 'dteacy2@mozilla.com',
    'isAdmin': false
  },
  {
    'id': 4,
    'documentNumber': '10225788',
    'firstName': 'Garald',
    'middleName': 'Ezri',
    'lastName': 'Tejada',
    'secondLastName': 'Breinlein',
    'email': 'ebreinlein3@arstechnica.com',
    'isAdmin': true
  }
];

import * as apiDm  from '../utils/api-dm.js';

export async function getUsers() {

  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, MOCK_USERS);
  } else {
    const response = await apiDm.getWithAuth('/users');
    return response;
  }
}

export async function getUserDetail(userId) {
  const { mock } = window.AppConfig;

  if (mock) {
    return apiDm.buildMockResponse(200, MOCK_USERS[userId - 1]);
  } else {
    const response = await apiDm.getWithAuth('/users/' + userId);
    return response;
  }
}

export async function saveUser(data) {
  const { mock } = window.AppConfig;
  if (mock) {
    return apiDm.buildMockResponse(201, {msg: 'Usuario mock creado'});
  } else {
    const response = await apiDm.postWithAuth('/users', data);
    return response;
  }
}

export async function updateUser(userId, data) {
  const { mock } = window.AppConfig;
  if (mock) {
    return apiDm.buildMockResponse(202, {msg: 'Usuario mock actualizado'});
  } else {
    const response = await apiDm.putWithAuth('/users/' + userId, data);
    return response;
  }
}
