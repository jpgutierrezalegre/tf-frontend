import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-list-action';
import '@bbva-web-components/bbva-button-floating-action';

import { getUsers } from '../../elements/users-dm/users-dm.js';

import '../../elements/header-ui/header-ui';
import { isLoggedIn } from '../../elements/auth-dm/auth-dm';

class UsersPage extends i18n(CellsPage) {
  static get is() {
    return 'users-page';
  }

  constructor() {
    super();

    this.users = [];
  }

  static get properties() {
    return {
      users: { type: Array },
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
  }

  onPageEnter() {
    if (!isLoggedIn()) {
      window.cells.logout();
    } else {

      this.subscribe('force_users_reload', () => this.users = []);

      if (!this.users.length) {
        getUsers().then((res) => {
          if (res.status === 200) {
            res.json().then(users=>{
              this.users = users;
            });
          } else {
            this.publish('handle-error', res);
          }
        });
      }
    }
  }

  onPageLeave() {
  }

  get usersList() {
    if (!this.users.length) {
      return null;
    }
    return this.users.map((user) => {
      const userProperties = this.buildUserProperties(user);

      return html`
        <bbva-list-action ...="${spread(userProperties)}">
        </bbva-list-action>
      `;
    });
  }

  buildUserProperties(user) {
    let props = {
      'icon': user.isAdmin ? 'coronita:executive' : 'coronita:myprofile',
      'no-closable': '',
      'main-title': [user.firstName, user.middleName, user.lastName, user.secondLastName].join(' '),
      'activation': user.documentNumber || '',
      'description-badge-text': user.isAdmin ? 'Administrador' : '',
      'accesibility-text-edit': 'Editar',
      '@list-action-edit-click': () => this._handleUserEdit(user)
    };

    if (user.isAdmin) {
      props = {
        ...props,
        'no-link': true
      };
    } else {
      props = {
        ...props,
        'href-link': window.location.href.replace('users', 'accounts?id=' + user.id)
      };
    }

    return props;
  }

  _handleUserEdit({id}) {
    this.navigate('user-detail', { id: id });
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">

        </div>
        <div slot="app__main" class="container">
          <header-component hide-back="" > </header-component>
          <div class="users-wrapper">
            ${this.usersList ? html`${this.usersList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
          </div>
          <div class="add-user-div">
            <bbva-button-floating-action variant="medium-blue" aria-label="Add user" icon="coronita:newclient" @click=${() => this.navigate('user-detail', {})}>
            </bbva-button-floating-action>
          </div>

        </div>
      </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      .users-wrapper {
        width: 50%;
        margin: auto;
      }
      .add-user-div {
        position: fixed;
        bottom: 0rem;
        right: 1rem;
        z-index: 1;
      }
    `;
  }
}

window.customElements.define(UsersPage.is, UsersPage);
