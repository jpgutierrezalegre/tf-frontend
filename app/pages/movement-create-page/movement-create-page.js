import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-expandable-multistep';
import '@bbva-web-components/bbva-expandable-step';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-form-select';
import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-modal-alert';

import '../../elements/header-ui/header-ui';
import { isAdmin, isLoggedIn } from '../../elements/auth-dm/auth-dm';
import { getAccounts, getAccountOwner } from '../../elements/accounts-dm/accounts-dm';
import { formatAmount } from '../../elements/utils/text';
import { createMovement } from '../../elements/accounts-dm/accounts-dm';

export class MovementCreatePage extends i18n(CellsPage) {

  static get is() {
    return 'movement-create-page';
  }

  constructor() {
    super();

    this.accounts = null;
    this.movementData = {};
    this.summary = {};
  }

  static get properties() {
    return {
      accounts: { type: Array },
      movementData: {type: Object },
    };
  }

  onPageEnter() {
    if (!isLoggedIn()) {
      window.cells.logout();
    }

    if (isAdmin()) {
      setTimeout(()=>{
        this.navigate('users', {});
      }, 250);
    } else if (!this.params.account) {
      setTimeout(()=>{
        this.navigate('accounts', {});
      }, 250);
    } else {
      getAccounts().then(res=>{
        if (res.status === 200) {
          res.json().then(accountsRes=>{
            this.accounts = accountsRes;
          });
        } else {
          this.publish('handle-error', res);
        }
      });
    }
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this.successModal = this.shadowRoot.querySelector('#successModal');

  }

  onPageLeave() {
    this.accounts = null;
    this.movementData = {};
  }

  handleContinue(e, currentStep) {
    let canContinue = true;

    switch (currentStep) {
      case 1:
        canContinue = this.validateFirstStep(e);
        break;
    }

    if (canContinue) {
      e.currentTarget.dispatchEvent(new CustomEvent('complete', { bubbles: true }));
    }
  }

  validateFirstStep(e) {
    const originAccountNumberInput = this.shadowRoot.querySelector('#originAccount');

    let isValid = true;

    if (originAccountNumberInput.value.length === 0) {
      originAccountNumberInput.invalid = true;
      isValid = false;
    }

    if (isValid) {
      this.movementData = {
        ...this.movementData,
        originAccount: originAccountNumberInput.value
      };
      this.summary = {
        ...this.summary,
        1: originAccountNumberInput.selectedOption.text
      };
      e.currentTarget.dispatchEvent(new CustomEvent('complete', { bubbles: true }));
    }
  }

  validateSecondStep(e) {
    const destinationAccountNumberInput = this.shadowRoot.querySelector('#destinationAccount');
    let isValid = true;
    [ destinationAccountNumberInput ].forEach((el) => (el.validate(), el.invalid && (isValid = false)));

    if (isValid) {

      const currentTarget = e.currentTarget;

      getAccountOwner(destinationAccountNumberInput.value).then(res=>{
        if (res.status === 200) {
          res.json().then(resJson=>{
            this.movementData = {
              ...this.movementData,
              destinationAccount: destinationAccountNumberInput.value
            };
            this.summary = {
              ...this.summary,
              2: destinationAccountNumberInput.value + ' - ' + [resJson.firstName, resJson.middleName, resJson.lastName, resJson.secondLastName].join(' '),
            };
            currentTarget.dispatchEvent(new CustomEvent('complete', { bubbles: true }));
          });
        } else {
          this.publish('handle-error', res);
        }
      });
    }
  }

  validateThirdStep(e) {
    const amountInput = this.shadowRoot.querySelector('#amount');
    const referenceInput = this.shadowRoot.querySelector('#reference');

    let isValid = true;

    [amountInput, referenceInput].forEach((el) => (el.validate(), el.invalid && (isValid = false)));

    let amountAsFloat = parseFloat(amountInput.value);

    if (isNaN(amountAsFloat)) {
      isValid = false;
      amountInput.invalid = true;
    }

    let originAccount = this.accounts.find(a=>a.accountNumber === this.movementData.originAccount);

    if (originAccount.balance < amountAsFloat) {
      isValid = false;
      amountInput.invalid = true;
    }

    if (isValid) {
      this.movementData = {
        ...this.movementData,
        amount: amountAsFloat,
        currency: originAccount.currency,
        reference: referenceInput.value
      };
      this.summary = {
        ...this.summary,
        3: formatAmount(originAccount.currency, amountAsFloat) + ' - ' + referenceInput.value
      };
      e.currentTarget.dispatchEvent(new CustomEvent('complete', { bubbles: true }));
    }

  }

  getStepSummary(step) {
    if (!this.movementData || !this.accounts) {
      return '';
    }
    return this.summary[step];
  }

  get firstStepSummary() {
    return this.getStepSummary(1);
  }

  get secondStepSummary() {
    return this.getStepSummary(2);
  }

  get thirdStepSummary() {
    return this.getStepSummary(3);
  }

  get summaryContent() {
    if (!this.movementData || Object.keys(this.movementData).length < 5) {
      return '';
    }

    let movement = this.movementData;
    return html`
      <div class="summary-col-content">
        <span>Cuenta Origen:</span>
        <span class="summary-value">${movement.originAccount}</span>
      </div>
      <div class="summary-col-content">
        <span>Cuenta Destino:</span>
        <span class="summary-value">${movement.destinationAccount}</span>
      </div>
      <div class="summary-col-content">
        <span>Monto:</span>
        <span class="summary-value">${formatAmount(movement.currency, movement.amount)}</span>
      </div>
      <div class="summary-col-content">
        <span>Referencia:</span>
        <span class="summary-value">${movement.reference}</span>
      </div>
    `;
  }

  handleFinish() {

    const data = {...this.movementData};
    delete data.originAccount;

    this.publish('display-loading', true);
    createMovement(this.movementData.originAccount, data).then(res=>{
      this.publish('display-loading', false);
      if (res.status === 201) {
        res.json().then(resJson=>{
          // remove footer button
          let footerButton = this.successModal.shadowRoot.getElementById('footer-button');
          if (footerButton) {
            footerButton.remove();
          }

          // remove X
          let modalHeader = this.successModal.shadowRoot.querySelector('.modal-header');
          if (modalHeader) {
            modalHeader.parentNode.removeChild(modalHeader);
          }

          this.successModal.open();
        });
      } else {
        this.publish('handle-error', res);
      }
    });
  }

  get accountsList() {
    if (!this.accounts) {
      return [];
    }
    return this.accounts.map(a=>{
      let props = {
        text: MovementCreatePage.getAccountDisplay(a),
        value: a.accountNumber,
      };

      if (a.accountNumber.toString() === this.params.account.toString()) {
        props = {
          ...props,
          selected: true
        };
      }

      return props;
    });
  }

  static getAccountDisplay(account) {
    return account.accountNumber + ' (' + formatAmount(account.currency, account.balance)  + ')';
  }

  handleSucessModalClick() {
    this.navigate('accounts');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">

        </div>
        <div slot="app__main" class="container">
          <header-component @header-back-click=${() => this.navigate('account-details', {account: this.params.account})} > </header-component>

          ${!this.accounts ? html`
            <cells-skeleton-loading-page visible></cells-skeleton-loading-page>
          ` : html`
            <bbva-expandable-multistep has-animation="" style="display: contents">
              <bbva-expandable-step class="animation" id="step-1" header="Origen" disabled-target=".action">
                <div class="content">
                  <div class="form__content">
                    <bbva-form-select id="originAccount" label="Cuenta Origen" required="" .items=${this.accountsList} error-message="Seleccione una cuenta origen">
                    </bbva-form-select>
                  </div>
                  <bbva-button-default class="action" text="Siguiente" @click=${(e)=>this.validateFirstStep(e)}></bbva-button-default>
                </div>
                <div class="summary" slot="summary">
                  <p>${this.firstStepSummary}</p>
                </div>
              </bbva-expandable-step>

              <bbva-expandable-step class="animation" id="step-2" header="Destino" disabled-target=".action">
                <div class="content">
                  <div class="form__content">
                    <bbva-form-field id="destinationAccount" label="Cuenta Destino" required error-message="Ingrese un número de cuenta">
                    </bbva-form-field>
                  </div>
                  <bbva-button-default class="action" text="Siguiente" @click=${(e)=>this.validateSecondStep(e)}></bbva-button-default>
                </div>
                <div class="summary" slot="summary">
                  <p>${this.secondStepSummary}</p>
                </div>
              </bbva-expandable-step>

              <bbva-expandable-step disable-on-previous-step-edit="" class="animation" id="step-3" header="Detalles" disabled-target=".action">
                <div class="content">
                  <div class="form__content">
                    <bbva-form-field id="amount" label="Monto" required error-message="Ingrese un monto válido">
                    </bbva-form-field>
                  </div>
                  <div class="form__content" style="margin-top: 15px">
                    <bbva-form-field id="reference" label="Referencia" required error-message="Ingrese una referencia">
                    </bbva-form-field>
                  </div>
                  <bbva-button-default class="action" text="Siguiente" @click=${(e)=>this.validateThirdStep(e)}></bbva-button-default>
                </div>
                <div class="summary" slot="summary">
                  <p>${this.thirdStepSummary}</p>
                </div>
              </bbva-expandable-step>

              <bbva-expandable-step class="animation" id="step-4" header="Resumen" disabled-target=".action">
                <div class="content">
                  ${this.summaryContent}
                  <bbva-button-default class="action" text="Realizar movimiento" @click=${()=>this.handleFinish()}></bbva-button-default>
                </div>
                <div class="summary" slot="summary">
                  <p>Summary 4</p>
                </div>
              </bbva-expandable-step>
            </bbva-expandable-multistep>
          `};

          <bbva-modal-alert id="successModal" header-text="" link-text="Ir a Mis Cuentas" @modal-footer-link-click=${()=>this.handleSucessModalClick()}>
            <div class="content" slot="slot-content-header">
              <cells-icon class="content-icon" size="24" icon="coronita:correct"></cells-icon>
              <p class="title">Movimiento realizado</p>
            </div>
            <div class="content" slot="slot-content">
              ${this.summaryContent}
              <div class="summary-col-content">
                <span>Fecha:</span>
                <span class="summary-value">${new Date().toLocaleDateString()}</span>
              </div>
            </div>
          </bbva-modal-alert>

        </div>
      </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      .content {
        text-align: center;
      }
      .form__content {
        width: 25rem;
        margin: auto;
        text-align: left;
      }
      .action {
        margin-top: 15px;
      }
      .summary-col-content {
        flex-direction: column;
        padding-top: 10px;
      }
      .summary-value {
        font-weight: 500;
      }
      .content-icon {
        color: green;
        margin-bottom: 1rem;
      }
    `;
  }

}

window.customElements.define(MovementCreatePage.is, MovementCreatePage);
