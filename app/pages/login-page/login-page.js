import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';

import { login } from '../../elements/login-dm/login-dm.js';

class LoginPage extends i18n(CellsPage) {
  static get is() {
    return 'login-page';
  }

  static get properties() {
    return {
      notificationData: { type: Object }
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main text="TechU Practitioner 2020">
          </bbva-header-main>
        </div>
        <div slot="app__main" class="container">
          <bbva-form-field id="user" auto-validate validate-on-blur label=${this.t('login-page.user-input')} required>
          </bbva-form-field>
      
          <bbva-form-password id="password" auto-validate validate-on-blur label=${this.t('login-page.password-input')}
            required>
          </bbva-form-password>
      
          <bbva-button-default @click=${this.handleValidation}>
            ${this.t('login-page.button')}
          </bbva-button-default>

          <div class="footer_copyright">
            © Juan Pablo Gutiérrez Alegre
            <br>
            BBVA TechU - Practitioner 2020 
          </div>
        </div>
      </cells-template-paper-drawer-panel>`;
  }

  handleValidation() {
    let canContinue = true;

    [this._userInput, this._passwordInput].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    if (canContinue) {

      this.publish('display-loading', true);
      login(this._userInput.value, this._passwordInput.value)
        .then((responseLogin)=> {
          this.publish('display-loading', false);
          if (responseLogin.status === 200) {
            responseLogin.json()
              .then(loginData=> {
                this.publish('loginSuccess', { name: loginData.name, token: loginData.token, isAdmin: loginData.isAdmin});
                if (loginData.isAdmin) {
                  this.navigate('users');
                } else {
                  this.navigate('accounts');
                }
              });
          } else {
            this.publish('handle-error', responseLogin);
          }
        }).catch(err=>{
          this.publish('display-loading', false);
          this.publish('display-notification', { type: 'error', icon: 'coronita:alert', text: 'Error general'});
        });

    }
  }

  onPageEnter() {
    // Cada vez que accedamos al login, simulamos una limpieza de los datos almacenados en memoria.
  }

  onPageLeave() {
    // Cada vez que salgamos del login, limpiamos las cajas de texto.
    setTimeout(() => [this._userInput, this._passwordInput].forEach((el) => el.clearInput()), 3 * 1000);
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding-top: 40px;
      }

      .container > * {
        margin-top: 25px;
      }

      .footer_copyright {
        position: fixed;
        bottom: 1rem;
        text-align: center
      }
    `;
  }
}

window.customElements.define(LoginPage.is, LoginPage);