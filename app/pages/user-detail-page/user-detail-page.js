import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-form-toggle';

import '../../elements/header-ui/header-ui';
import { isLoggedIn } from '../../elements/auth-dm/auth-dm';

import { getUserDetail } from '../../elements/users-dm/users-dm.js';
import { saveUser } from '../../elements/users-dm/users-dm.js';
import { updateUser } from '../../elements/users-dm/users-dm.js';

class UserDetailPage extends i18n(CellsPage) {
  static get is() {
    return 'user-detail-page';
  }

  constructor() {
    super();
    this.isReady = false;
  }

  static get properties() {
    return {
      isReady: { type: Boolean },
      userData: { type: Object }
    };
  }

  onPageEnter() {
    this.isReady = false;
    if (!isLoggedIn()) {
      window.cells.logout();
    } else {
      if (this.params.id) {
        getUserDetail(this.params.id).then(res => {
          if (res.status === 200) {
            res.json().then(resJson => {
              this.userData = resJson;
              this.isReady = true;
            });
          } else {
            this.publish('handle-error', res);
          }
        });
      } else {
        setTimeout(() => {
          this.userData = null;
          this.isReady = true;
        }, 10);
      }
    }
  }

  onPageLeave() {
    this.params = {};
  }

  getFieldValue(field) {
    return this.userData ? { value: this.userData[field] } : {};
  }

  getAdministratorToggleProperties() {
    if (!this.userData) {
      return {};
    } else {
      let props = {
        disabled: '',
      };
      if (this.userData.isAdmin) {
        props = {
          ...props,
          checked: '',
        };
      }
      return props;
    }
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
        </div>

        <div slot="app__main" class="container">
          <header-component @header-back-click=${() => this.navigate('users')}> </header-component>

          <div class="fieldsContainer">

          ${this.isReady ? html`

            <div style="display: flex">
              <p style="margin-right: 20px">¿Administrador?</p>
              <bbva-form-toggle id="isAdmin" ...="${spread(this.getAdministratorToggleProperties())}"></bbva-form-toggle>
            </div>

            <bbva-form-field id="documentNumber" label="Numero documento" required ...="${spread(this.getFieldValue('documentNumber'))}">
            </bbva-form-field>

            <bbva-form-field id="firstName" label="Primer Nombre" required ...="${spread(this.getFieldValue('firstName'))}">
            </bbva-form-field>

            <bbva-form-field id="middleName" label="Segundo Nombre" ...="${spread(this.getFieldValue('middleName'))}">
            </bbva-form-field>

            <bbva-form-field id="lastName" label="Apellido paterno" required ...="${spread(this.getFieldValue('lastName'))}">
            </bbva-form-field>

            <bbva-form-field id="secondLastName" label="Apellido materno" ...="${spread(this.getFieldValue('secondLastName'))}">
            </bbva-form-field>

            <bbva-form-field id="email" label="Correo electrónico" required ...="${spread(this.getFieldValue('email'))}">
            </bbva-form-field>

            <bbva-form-password id="password" label="Contraseña" ...="${spread(this.userData ? {} : { required: '' })}" >
            </bbva-form-password>

            <bbva-form-password id="confirmPassword" label="Confirmar contraseña" ...="${spread(this.userData ? {} : { required: '' })}">
            </bbva-form-password>

            <bbva-button-default @click=${this.handleValidation}>
              ${this.userData ? 'Actualizar' : 'Crear' } 
            </bbva-button-default>
          ` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}

          </div>
          
        </div>
      </cells-template-paper-drawer-panel>`;
  }

  handleValidation() {
    let canContinue = true;

    const documentNumberInput = this.shadowRoot.querySelector('#documentNumber');
    const firstNameInput = this.shadowRoot.querySelector('#firstName');
    const middleNameInput = this.shadowRoot.querySelector('#middleName');
    const lastNameInput = this.shadowRoot.querySelector('#lastName');
    const secondLastNameInput = this.shadowRoot.querySelector('#secondLastName');
    const emailInput = this.shadowRoot.querySelector('#email');
    const isAdminInput = this.shadowRoot.querySelector('#isAdmin');

    const passwordInput = this.shadowRoot.querySelector('#password');
    const confirmPasswordInput = this.shadowRoot.querySelector('#confirmPassword');

    // check mandatory
    const mandatoryFields = [documentNumberInput, firstNameInput, lastNameInput, emailInput];

    if (this.userData === null) {
      mandatoryFields.push(passwordInput, confirmPasswordInput);
    }

    mandatoryFields.forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    // check passwords (if apply)
    if ((passwordInput.value || confirmPasswordInput.value) && passwordInput.value !== confirmPasswordInput.value) {
      canContinue = false;
      this.publish('display-notification', { type: 'error', icon: 'coronita:alert', text: 'Contraseñas no coinciden'});
    }

    if (canContinue) {
      const userData = {
        documentNumber: documentNumberInput.value,
        firstName: firstNameInput.value,
        middleName: middleNameInput.value,
        lastName: lastNameInput.value,
        secondLastName: secondLastNameInput.value,
        email: emailInput.value,
        password: passwordInput.value,
        isAdmin: isAdminInput.checked,
      };

      let processResult = (res, statusOk) => {
        this.publish('display-loading', false);
        if (res.status === statusOk) {
          res.json().then(resJson => {
            this.publish('display-notification', { type: 'success', icon: 'coronita:correct', text: resJson.msg});
            this.publish('force_users_reload');
            this.navigate('users');
          });
        } else {
          this.publish('handle-error', res);
        }
      };

      this.publish('display-loading', true);
      if (this.userData) {
        // update user
        updateUser(this.userData.id, userData).then(res => {
          processResult(res, 202);
        });
      } else {
        // new user
        saveUser(userData).then(res => {
          processResult(res, 201);
        });
      }
    }

  }

  static get styles() {
    return css`

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .fieldsContainer {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .fieldsContainer > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(UserDetailPage.is, UserDetailPage);
