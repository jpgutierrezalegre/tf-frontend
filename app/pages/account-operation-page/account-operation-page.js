import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-form-radio-group';
import '@bbva-web-components/bbva-form-radio-button';
import '@bbva-web-components/bbva-modal-alert';

import '../../elements/header-ui/header-ui';
import { isLoggedIn, isAdmin } from '../../elements/auth-dm/auth-dm';
import { formatAmount } from '../../elements/utils/text';
import { createAccountOperation } from '../../elements/accounts-dm/accounts-dm';

class AccountOperationPage extends i18n(CellsPage) {
  static get is() {
    return 'account-operation-page';
  }

  constructor() {
    super();
    this.isReady = false;
    this.summaryData = null;
  }

  static get properties() {
    return {
      isReady: { type: Boolean },
      summaryData: { type: Object },
    };
  }

  onPageEnter() {
    if (!isLoggedIn()) {
      window.cells.logout();
    } else {
      if (!isAdmin()) {
        setTimeout(()=>{
          this.navigate('accounts', {});
        }, 250);
      } else if (!this.params.id) {
        // admin but not selected user
        setTimeout(()=>{
          this.navigate('users', {});
        }, 250);
      } else {
        setTimeout(() => {
          this.isReady = true;
        }, 10);
      }
    }
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this.successModal = this.shadowRoot.querySelector('#successModal');
  }

  onPageLeave() {
    this.summaryData = null;
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
        </div>

        <div slot="app__main" class="container">
          <header-component @header-back-click=${() => this.navigate('account-details', {account: this.params.account, id: this.params.id})} > </header-component>

          <div class="fieldsContainer">
          
          ${this.isReady ? html`
            <bbva-form-field label="Número de cuenta" value=${this.params.account} readonly="">
            </bbva-form-field>

            <bbva-form-radio-group id="operationType" selected="0">
              <bbva-form-radio-button value="1" divider="full">
                Depósito
              </bbva-form-radio-button>
              <bbva-form-radio-button value="2" divider="full">
                Retiro
              </bbva-form-radio-button>
            </bbva-form-radio-group>

            <bbva-form-field id="amount" label="Monto" required error-message="Ingrese un monto válido">
            </bbva-form-field>

            <bbva-button-default @click=${this.handleValidation}>
              Procesar
            </bbva-button-default>
          ` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
          
          </div>

          <bbva-modal-alert id="successModal" header-text="" link-text="Ir a Cuentas" @modal-footer-link-click=${()=>this.handleSucessModalClick()}>
            <div class="content" slot="slot-content-header">
              <cells-icon class="content-icon" size="24" icon="coronita:correct"></cells-icon>
              <p class="title">Operación realizada</p>
            </div>
            <div class="content" slot="slot-content">
              ${this.summaryContent}
            </div>
          </bbva-modal-alert>
          
        </div>
      </cells-template-paper-drawer-panel>`;
  }

  handleSucessModalClick() {
    this.navigate('accounts', {id: this.params.id});
  }

  handleValidation() {
    let canContinue = true;

    const amountInput = this.shadowRoot.querySelector('#amount');
    const operationTypeVal = this.shadowRoot.querySelector('#operationType').selected;

    // check mandatory
    [ amountInput ].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    let amountValue = parseFloat(amountInput.value);

    if (isNaN(amountValue)) {
      canContinue = false;
      amountInput.invalid = true;
    }

    if (operationTypeVal === 1) {
      amountValue *= -1;
    }

    if (canContinue) {
      this.publish('display-loading', true);
      createAccountOperation(this.params.account, {amount: amountValue}).then(res=>{
        this.publish('display-loading', false);
        if (res.status === 200) {

          this.summaryData = {
            account: this.params.account,
            operationType: operationTypeVal,
            amount: parseFloat(amountInput.value)
          };

          // remove footer button
          let footerButton = this.successModal.shadowRoot.getElementById('footer-button');
          if (footerButton) {
            footerButton.remove();
          }

          // remove X
          let modalHeader = this.successModal.shadowRoot.querySelector('.modal-header');
          if (modalHeader) {
            modalHeader.parentNode.removeChild(modalHeader);
          }

          this.successModal.open();
        } else {
          this.publish('handle-error', res);
        }
      });
    }

  }

  get summaryContent() {
    if (!this.summaryData) {
      return '';
    }

    let operation = this.summaryData;
    return html`
      <div class="summary-col-content">
        <span>Cuenta:</span>
        <span class="summary-value">${operation.account}</span>
      </div>
      <div class="summary-col-content">
        <span>Operación:</span>
        <span class="summary-value">${operation.operationType === 0 ? 'Depósito' : 'Retiro'}</span>
      </div>
      <div class="summary-col-content">
        <span>Monto:</span>
        <span class="summary-value">${formatAmount('PEN', operation.amount)}</span>
      </div>
      <div class="summary-col-content">
        <span>Fecha:</span>
        <span class="summary-value">${new Date().toLocaleDateString()}</span>
      </div>
    `;
  }

  static get styles() {
    return css`

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .fieldsContainer {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .fieldsContainer > * {
        margin-top: 10px;
      }

      .summary-col-content {
        flex-direction: column;
        padding-top: 10px;
      }
      .summary-value {
        font-weight: 500;
      }
      .content-icon {
        color: green;
        margin-bottom: 1rem;
      }
    `;
  }
}

window.customElements.define(AccountOperationPage.is, AccountOperationPage);
