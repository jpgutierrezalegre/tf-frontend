import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-button-default';

import '../../elements/header-ui/header-ui';
import { isLoggedIn, isAdmin } from '../../elements/auth-dm/auth-dm';

import { createAccount } from '../../elements/accounts-dm/accounts-dm';

class AccountAddPage extends i18n(CellsPage) {
  static get is() {
    return 'account-add-page';
  }

  constructor() {
    super();
    this.isReady = false;
  }

  static get properties() {
    return {
      isReady: { type: Boolean },
    };
  }

  onPageEnter() {
    if (!isLoggedIn()) {
      window.cells.logout();
    } else {
      if (!isAdmin()) {
        setTimeout(()=>{
          this.navigate('accounts', {});
        }, 500);
      } else if (!this.params.id) {
        // admin but not selected user
        setTimeout(()=>{
          this.navigate('users', {});
        }, 500);
      } else {
        setTimeout(() => {
          this.isReady = true;
        }, 10);
      }
    }
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
        </div>

        <div slot="app__main" class="container">
          <header-component @header-back-click=${() => this.navigate('accounts', (isAdmin() ? {id: this.params.id} : {}))}> </header-component>

          <div class="fieldsContainer">
          
          ${this.isReady ? html`
            <bbva-form-field id="accountNumber" label="Número cuenta" required error-message="Ingrese un número de cuenta">
            </bbva-form-field>

            <bbva-button-default @click=${this.handleValidation}>
              Crear
            </bbva-button-default>
          ` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}

          </div>
          
        </div>
      </cells-template-paper-drawer-panel>`;
  }

  handleValidation() {
    let canContinue = true;

    const accountNumberInput = this.shadowRoot.querySelector('#accountNumber');

    // check mandatory
    [ accountNumberInput ].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    if (canContinue) {
      let accountData = {
        accountNumber: accountNumberInput.value,
        currency: 'PEN',
      };

      if (isAdmin) {
        accountData = {
          ...accountData,
          userId: parseInt(this.params.id)
        };
      }

      this.publish('display-loading', true);
      createAccount(accountData).then(res=>{
        this.publish('display-loading', false);
        if (res.status === 201) {
          res.json().then(resJson => {
            this.publish('display-notification', { type: 'success', icon: 'coronita:correct', text: resJson.msg});
            this.publish('force_accounts_reload');
            this.navigate('accounts', isAdmin() ? {id: this.params.id} : {});
          });
        } else {
          this.publish('handle-error', res);
        }
      });
    }

  }

  static get styles() {
    return css`

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .fieldsContainer {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .fieldsContainer > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(AccountAddPage.is, AccountAddPage);
