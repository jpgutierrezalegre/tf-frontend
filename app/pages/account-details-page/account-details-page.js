import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-button-floating-action';
import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-panel-detail';

import '../../elements/header-ui/header-ui';
import { isAdmin, isLoggedIn } from '../../elements/auth-dm/auth-dm';
import { displayDate, formatAmount } from '../../elements/utils/text';
import { getAccountDetails } from '../../elements/accounts-dm/accounts-dm.js';

class AccountDetailsPage extends i18n(CellsPage) {
  static get is() {
    return 'account-details-page';
  }

  constructor() {
    super();

    this.account = null;
    this.modalDetailsData = null;
  }

  static get properties() {
    return {
      account: { type: Object },
      modalDetailsData: {type: String},
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._detailsModal = this.shadowRoot.querySelector('#detailsModal');
  }

  onPageEnter() {
    if (!isLoggedIn()) {
      window.cells.logout();
    } else {

      this.subscribe('force_account_details_reload', () => this.account = null);

      if (!this.account) {
        getAccountDetails(this.params.account).then((res) => {
          if (res.status === 200) {
            res.json().then(accountRes=>{
              this.account = accountRes;
            });
          } else {
            this.publish('handle-error', res);
          }
        });
      }
    }
  }

  onPageLeave() {
    this.account = null;
    this.modalDetailsData = null;
  }

  get accountMovementsList() {
    if (!this.account) {
      return null;
    }

    if (this.account.movements.length === 0) {
      return html`<p style="text-align: center;">No se encontraron movimientos.</p>`;
    }

    return this.account.movements.map((movement) => {
      const movementProperties = this.buildMovementProperties(movement);

      return html`
        <bbva-list-movement ...="${spread(movementProperties)}">
        </bbva-list-movement>
      `;
    });
  }

  buildMovementProperties(movement) {
    if (movement.originAccount === this.account.accountNumber) {
      movement.fromThisAccount = true;
    }

    return {
      'class': 'bbva-global-semidivider',
      'aria-label': 'Ver detalle del movimiento',
      'language': 'es',
      'card-title': movement.reference,
      'amount': movement.fromThisAccount ? movement.amount * -1 : movement.amount,
      'local-currency': movement.currency,
      'currency-code': movement.currency,
      'description': displayDate(movement.date),
      'mask': movement.fromThisAccount ? movement.destinationAccount : movement.originAccount,
      '@click': () => this.handleMovementClick(movement),
    };

  }

  handleMovementClick(movement) {
    this.modalDetailsData = movement;
    this._detailsModal.open();
  }

  get modalDetailsContent() {
    if (!this.modalDetailsData) {
      return html ``;
    }

    const movement = this.modalDetailsData;

    return html`
      <div class="col-content">
        <span>Referencia:</span>
        <span class="value">${movement.reference}</span>
      </div>
      <div class="col-content">
        <span>Fecha:</span>
        <span class="value">${displayDate(movement.date)}</span>
      </div>
      <div class="col-content">
        <span>Monto:</span>
        <span class="value">${formatAmount(movement.currency, movement.amount)}</span>
      </div>

      ${ movement.destinationAccount && movement.originAccount ? html`
        <div class="col-content">
          <span>Cuenta ${movement.fromThisAccount ? 'Destino' : 'Origen'}:</span>
          <span class="value">${movement.fromThisAccount ? movement.destinationAccount : movement.originAccount}</span>
        </div>
      ` : html`` }
    `;

  }

  get accountDetailsPanelProperties() {
    let props = this.account ? {
      'card-title': 'Nro. Cuenta: ' + this.account.accountNumber,
      'amount': this.account.balance,
      'local-currency': this.account.currency,
      'currency-code': this.account.currency,
    } : {};
    return props;
  }

  get addButtonProperties() {
    if (!this.account) {
      return {};
    }
    if (isAdmin()) {
      return {
        'aria-label': 'Add operation',
        'icon': 'coronita:transfer',
        '@click': () => this.navigate('account-operation', {account: this.account.accountNumber, id: this.params.id}),
      };
    } else {
      return {
        'aria-label': 'Add movement',
        'icon': 'coronita:transfertoaccount',
        '@click': () => this.navigate('movement-create', {account: this.account.accountNumber}),
      };
    }
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">

        </div>
        <div slot="app__main" class="container">
          <header-component @header-back-click=${() => this.navigate('accounts', (isAdmin() ? {id: this.params.id} : {}))}> </header-component>
          ${!this.account ? html`
            <cells-skeleton-loading-page visible></cells-skeleton-loading-page>
          ` : html`
            <div class="account-info">
              <bbva-panel-detail ...="${spread(this.accountDetailsPanelProperties)}">
              </bbva-panel-detail>
            </div>
            <div class="movements-wrapper">
              ${this.accountMovementsList}
            </div>
            <div class="add-movement-div">
              <bbva-button-floating-action variant="medium-blue" ...="${spread(this.addButtonProperties)}"}>
              </bbva-button-floating-action>
            </div>
          `}

          <bbva-help-modal id="detailsModal" header-icon="coronita:info"
          header-text="Detalles de movimiento"
            style="z-index: 1000"
            link-text="Regresar">
            <div slot="slot-content">
              ${this.modalDetailsContent}
            </div>
          </bbva-help-modal>

        </div>
      </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      .account-info {
        position: sticky;
        top: 3rem;
        background: white;
      }
      .movements-wrapper {
        width: 40%;
        margin: auto;
        margin-top: 10px;
      }
      .add-movement-div {
        position: fixed;
        bottom: 0rem;
        right: 1rem;
        z-index: 1;
      }
      .col-content {
        flex-direction: column;
        padding-top: 10px;
      }
      .value {
        font-weight: 500;
      }
    `;
  }
}

window.customElements.define(AccountDetailsPage.is, AccountDetailsPage);
