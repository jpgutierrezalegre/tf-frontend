import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-action';
import '@bbva-web-components/bbva-button-floating-action';
import '@bbva-web-components/bbva-help-modal';

import '../../elements/header-ui/header-ui';
import { isLoggedIn, isAdmin } from '../../elements/auth-dm/auth-dm';
import { formatAmount, displayDate } from '../../elements/utils/text';
import { getAccounts, getAccountsFromUser, deleteAccount } from '../../elements/accounts-dm/accounts-dm.js';

class AccountsPage extends i18n(CellsPage) {
  static get is() {
    return 'accounts-page';
  }

  constructor() {
    super();

    this.accounts = null;
    this.modalDeleteData = null;
  }

  static get properties() {
    return {
      accounts: { type: Array },
      modalDeleteData: {type: String},
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._confirmModal = this.shadowRoot.querySelector('#confirmModal');
  }

  onPageEnter() {
    if (!isLoggedIn()) {
      window.cells.logout();
    } else {

      if (isAdmin() && !this.params.id) {
        // admin but not selected user
        setTimeout(()=>{
          this.navigate('users', {});
        }, 500);
      } else {

        this.subscribe('force_accounts_reload', () => this.accounts = null);

        this._getAccounts();
      }
    }
  }

  onPageLeave() {
    this.accounts = null;
  }

  _getAccounts() {
    let processResponse = (res) => {
      if (res.status === 200) {
        res.json().then(accounts=>{
          this.accounts = accounts;
        });
      } else {
        this.publish('handle-error', res);
      }
    };
    if (isAdmin()) {
      getAccountsFromUser(this.params.id).then(processResponse);
    } else {
      getAccounts().then(processResponse);
    }
  }

  get accountsList() {
    if (!this.accounts) {
      return null;
    }

    if (this.accounts.length === 0) {
      return html`<p style="text-align: center;">No se encontraron cuentas.</p>`;
    }

    return this.accounts.map((account) => {
      const accountProperties = this.buildAccountProperties(account);

      return html`
        <bbva-list-action ...="${spread(accountProperties)}">
        </bbva-list-action>
      `;
    });
  }

  buildAccountProperties(account) {
    let props = {
      'icon': 'coronita:account',
      'no-editable': '',
      'main-title': account.accountNumber,
      'activation': formatAmount(account.currency, account.balance),
      'connection': 'Apertura: ' + displayDate(account.openingDate),
      'href-link': window.location.href.replace('accounts', 'account-details/' + account.accountNumber),
    };

    if (isAdmin()) {
      props = {
        ...props,
        'description-badge-text': account.status === 1 ? 'Activa' : 'Inactiva',
        'badge-class': account.status === 1 ? 'success' : 'error',
        'accesibility-text-close': 'Desactivar',
        '@list-action-close-click': () => this._confirmDelete(account)
      };

      if (account.status !== 1) {
        props = {
          ...props,
          'no-closable': '',
        };
      }

    } else {
      props = {
        ...props,
        'no-closable': '',
        'description-badge-text': '',
      };
    }

    return props;
  }

  get confirmModalEvent() {
    if (!this.modalDeleteData) {
      return null;
    }
    return {
      '@help-modal-footer-button-click': () => this._handleDelete(this.modalDeleteData)
    };
  }

  _confirmDelete(account) {
    if (account.balance > 0) {
      this.publish('display-notification', { type: 'error', icon: 'coronita:alert', text: 'La cuenta tiene saldo a favor.'});
    } else {
      this.modalDeleteData = account.accountNumber;
      this._confirmModal.open();
    }
  }

  _handleDelete(accountNumber) {
    this.publish('display-loading', true);
    deleteAccount(accountNumber).then(res=>{
      this.publish('display-loading', false);
      if (res.status === 200) {
        res.json().then(resJson => {
          this.publish('display-notification', { type: 'success', icon: 'coronita:correct', text: resJson.msg});
          this.accounts = null;
          this._getAccounts();
        });
      } else {
        this.publish('handle-error', res);
      }
    });
  }

  get headerProperties() {
    if (isLoggedIn() && isAdmin()) {
      return {
        '@header-back-click': () => this.navigate('users')
      };
    }
    return {
      'hide-back': ''
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">

        </div>
        <div slot="app__main" class="container">
          <header-component ...="${spread(this.headerProperties)}"> </header-component>
          <div class="accounts-wrapper">
            ${this.accountsList ? html`${this.accountsList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
          </div>
          ${isLoggedIn() && isAdmin() ? html`
            <div class="add-account-div">
              <bbva-button-floating-action variant="medium-blue" aria-label="Add account" icon="coronita:add" @click=${() => this.navigate('account-add', {id: this.params.id})}>
              </bbva-button-floating-action>
            </div>
          ` : html``}          

          <bbva-help-modal id="confirmModal" header-icon="coronita:info"
            header-text="Eliminar cuenta"
            button-text=${this.t('dashboard-page.logout-modal.button')} 
            ...="${spread(this.confirmModalEvent)}"
            style="z-index: 1000">
            <div slot="slot-content">
              <span>¿Seguro de eliminar cuenta ${ this.modalDeleteData }?</span>
            </div>
          </bbva-help-modal>

        </div>
      </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      .accounts-wrapper {
        width: 40%;
        margin: auto;
      }
      .add-account-div {
        position: fixed;
        bottom: 0rem;
        right: 1rem;
        z-index: 1;
      }
    `;
  }
}

window.customElements.define(AccountsPage.is, AccountsPage);
